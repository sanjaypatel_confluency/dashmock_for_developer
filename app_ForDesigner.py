import dash
import dash_core_components as dcc
import dash_html_components as html
import dash_bootstrap_components as dbc
#import pandas as pd
#import numpy as np

#from working import df_WAMR_Nodes, df_PS_DischargeNodes, df_time_series, df_scenario, df_Output_With_Scen_Labels
#from working import setuplists, process_db_constraints, process_PS_violations_dataframe, process_WAMR_violation_dataframe, create_spatial_violation_dataframe, process_source_chem_costs_df, create_source_volume_charts, create_source_chemcial_charts 
#from working import create_map_box, process_ts_dataframe, create_ts_percentile, process_agg_df, create_energy_by_facility, process_breakdown_kWh_reductions_df, create_breakdown_kWh_reductions, create_kW_demand_analysis_charts, create_map_box_differences, create_subplots

from plotly_code import create_map_box_chart_static, create_bar_chart_static

from dash.dependencies import Input, Output
from plotly import graph_objs as go
from plotly.graph_objs import *
from datetime import datetime as dt


app = dash.Dash(
    __name__, external_stylesheets=[dbc.themes.DARKLY], meta_tags=[{"name": "viewport", "content": "width=device-width"}]
)
server = app.server

navbar = dbc.Navbar(
    [
            dbc.Row(
                [
                    dbc.Col(dbc.NavbarBrand("Confluency BlueIQ for GLWA", className="ml-2")),
                ],
                align="center",
                no_gutters=True,
        ),
    ],
    color="dark",
    dark=True,
)

# Layout of Dash App
app.layout = html.Div(
    children=[navbar,
        dbc.Container([
            dbc.Row([
                dbc.Col([
                
                    dbc.Row([
                        dbc.Col([
                            dbc.Label("Model Selection"),
                            dcc.Dropdown(
                                id="model_selection",
                                options=[
                                    {"label": i, "value": i}
                                    for i in ['Option1', 'Option2'] #df_Output_With_Scen_Labels['label'].unique()
                                ],
                                placeholder="Select a comparison day",
                                style={'marginTop': '0px'}
                            )
                        ], sm=6,md=4,lg=4),
                        dbc.Col([
                            dbc.Label("Simulation Type"),
                            dbc.RadioItems(
                                id="simulation_type",
                                options=[
                                    {'label': 'Compare Simulations', 'value': 'Compare'},
                                    {'label': 'Single Simulation', 'value': 'Single'},
                                ],
                                value='Compare',
                                style={'marginTop': '0px'},
                                ),
                        ], sm=6,md=4, lg=4),

                        dbc.Col([
                            dbc.Label("Metric"),
                            dbc.RadioItems(
                                id="metrics",
                                options=[
                                    {'label': 'kWh', 'value': 'kWh'},
                                    {'label': 'Volume', 'value': 'Vol'},
                                    {'label': 'Chemical Costs', 'value': 'CC'},
                                    {'label': 'On-Peak kW Demand', 'value': 'PeakDemand'},
                                    {'label': 'Anytime Demand', 'value': 'AnytimeDemand'},
                                ],
                                value='kWh',
                                style={'marginTop': '0px'}
                            ),
                        ], sm=6,md=4, lg=4)
                    ],
                    style={'marginLeft': '0px'}#, 'border': '2px grey solid'},
                    ),
                    
                    dbc.Row([
                        dbc.Col([
                            dcc.Graph(id="histogram1",style={'height': '40vh'})
                        ],lg=6,sm=12),

                        dbc.Col([
                            dcc.Graph(id="histogram2",style={'height': '40vh'})
                        ],lg=6,sm=12)
                    ],no_gutters=True),

                    dbc.Row([
                        ### THIS IS WHERE THE TIMESERIES GRAPH WILL GO
                        dbc.Col([
                            dcc.Graph(
                                id='example-graph',
                                figure=create_bar_chart_static(),
                                style={'height': '42vh'}
                                # style={'width': '90vh'}
                            )

                        ],width=12)
                    ]),
                ],  
                lg=7,sm= 12
                ),

                    dbc.Col([
                        dbc.Row([      
                            dbc.Col([
                                dcc.Dropdown(
                                    id="model_selection2",
                                    options=[
                                        {"label": i, "value": i}
                                        for i in ['Option1', 'Option2'] #df_Output_With_Scen_Labels['label'].unique()
                                    ],
                                    placeholder="Select a comparison day",
                                    style = {'width': '50%'},
                                ),
                            ],
                            ),               
                        ]),
                        dbc.Row([
                                dcc.Graph(id="map-graph",style={'height': '91vh'}),
                                ])
                            ], 
                        lg=5,sm=12,
                        style={'marginRight': '0px'},
                        #, 'border': '2px grey solid'},
                    ),  
                ],
                no_gutters=False, 
            ),
        ],
        fluid=True)

    ]
)


# Clear Selected Data if Click Data is used
@app.callback(Output("histogram1", "selectedData"), [Input("histogram1", "clickData")])
def update_selected_data(clickData):
    if clickData:
        return {"points": []}


# Update Histogram Figure based on Month, Day and Times Chosen
@app.callback(
    Output("histogram2", "figure"),
    [Input("simulation_type", "value"), Input("model_selection", "value"), Input("metrics", "value")],
)
def update_histogram2(simulation_type, model_selection, metrics):
    return create_bar_chart_static(True)


# Update Histogram Figure based on Month, Day and Times Chosen
@app.callback(
    Output("histogram1", "figure"),
    [Input("simulation_type", "value"), Input("model_selection", "value"), Input("metrics", "value")],
)
def update_histogram1(simulation_type, model_selection, metrics):
    return create_bar_chart_static()
    
# Update Map Graph based on date-picker, selected data on histogram and location dropdown
@app.callback(
    Output("map-graph", "figure"),
    [Input("simulation_type", "value"), Input("model_selection", "value"), Input("metrics", "value")],
)
def update_graph(simulation_type, model_selection, metrics):
    return create_map_box_chart_static()
    #return create_map_box(final_df)
    
    
if __name__ == "__main__":
    app.run_server(debug=True)