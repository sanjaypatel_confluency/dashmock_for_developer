import datetime
import os,sys
import pandas as pd
import plotly 
import plotly.graph_objs as go
MAPBOX_KEY = 'pk.eyJ1IjoibWFzb250aHJvbmVidXJnIiwiYSI6ImNqcHFhOWxiZDBhdXU0Mm9hN3pkM3hpYnAifQ.VWnyZVsEEN16JPNodl9CPQ'
lat = 41.6
long = -82.01


def create_map_box_chart_static():
    WAMRViolationSummary_Spatial_Full = pd.read_csv(r'supplementary\WAMRViolationSummary_Spatial_Full.csv')

    #AvailableLabels = WAMRViolationSummary_Spatial_Full['label'].unique()

    label = 'BlueIQ_i7_Day1.inp'

    dw_source = WAMRViolationSummary_Spatial_Full.loc[(WAMRViolationSummary_Spatial_Full['label']==label)]

    kwargs = {'lat':lat, 'long':long, 'color':'Violation_Full', 'colorscale': 'Oryel', 'marker_size': 20, 'opacity': 1,
                        'zoom': 9, 'showscale': True, 'colorbar_title': 'Exceeds (PSI)', 'hoverinfo': 'text', 'hovertext_series': 'hovertext',
                        'mode': 'markers', 'layout_showlegend': False, 'title': 'Pressure Violations - Average Exceedance(PSI) Per Time Step', 'style': 'dark'
                        }

    #setup layout
    layout = go.Layout()
    layout.mapbox=go.layout.Mapbox(accesstoken=MAPBOX_KEY,
                            #style = kwargs.get('style', 'plotly_dark'),               
                            bearing=0,                                  
                            pitch = 0,                                  
                            zoom = kwargs['zoom'],                 
                            center=go.layout.mapbox.Center(
                                                        lat=kwargs['lat'],          
                                                        lon=kwargs['long']   
                                                        ),
                            style=kwargs.get('style', 'Normal')
                            )

    #create data
    data = [] 
    hovertext_series = None
    if 'hovertext_series' in kwargs:
        hovertext_series = dw_source['hovertext']       

    data.append(go.Scattermapbox(
                    lat=dw_source['lat_x'],
                    lon=dw_source['long_x'],
                    text=hovertext_series,
                    #mode is actually set by default through plotly.config.json (call to synth_plotly_config at end of create_data)
                    #mode='markers', #v0.0.11 this is now handled without direct specification through update_data_by_kwargs
                    marker=go.scattermapbox.Marker(
                        size=kwargs['marker_size'],
                        color = dw_source['Violation_Full'],
                        colorscale = kwargs['colorscale'],
                        colorbar = dict(
                            title=dict(text=kwargs['colorbar_title'], font=dict(color='white', size=20), side='right'),
                            thickness=kwargs.get('colorbar_thickness', 20),
                            tickfont=dict(color='white', size=20),
                        ),
                        showscale=kwargs['showscale'],    #v0.0.12
                        ),
                    line = dict(
                        color = kwargs.get('line_color', 'black'),
                        width = kwargs.get('line_width', 10)
                        ),
                    )
                )

    ScatterMapBox = {'data':data, 'layout':layout}

    #set the mid point to be zero
    ScatterMapBox['data'][0]['marker']['cmid'] = 0
    #ScatterMapBox['data'][0]['marker']['cmax'] = 5
    #ScatterMapBox['data'][0]['marker']['cmin'] = -5

    ScatterMapBox['layout']['autosize'] = True
    ScatterMapBox['layout']['margin']=go.layout.Margin(l=0, r=0, t=0, b=0)

    return ScatterMapBox
    #plotly.offline.iplot(ScatterMapBox)


def create_bar_chart_static(diff = False):
    df_Dmd_Pk = pd.read_csv(r'supplementary\PeakDemand.csv').set_index('PS')
    df_Dmd_Pk['Peak_kW_Change'] = df_Dmd_Pk['BlueIQ_i7_Day1.inp'] - df_Dmd_Pk['Baseline_Day1.inp']

    #layout
    layout = go.Layout()
    layout.template = 'plotly_dark'
    layout.legend.orientation = 'h'
    layout.paper_bgcolor='#1E1E1E' #layout.paper_bgcolor='rgba(0,0,0,0)' layout.paper_bgcolor=
    layout.plot_bgcolor='#1E1E1E' #layout.plot_bgcolor='rgba(0,0,0,0)'

    if diff:
        dw_source = df_Dmd_Pk[['Peak_kW_Change']]
        layout.title = 'Change in Values'
        layout.yaxis.title = 'Change in Highest On-Peak kW Demand'
        colorarray = ['#fde725']
    else:
        #get baseline and blueIQ final solutions only
        dw_source = df_Dmd_Pk[['Baseline_Day1.inp', 'BlueIQ_i7_Day1.inp']]
        layout.title = 'Highest On-Peak kW Demand'
        layout.yaxis.title = 'Highest On-Peak kW Demand' 
        colorarray = ['rgb(5,10,172)', 'rgb(220,220,220)']

    #data
    data = []
    columns = dw_source.columns
    x = dw_source.index.values
    
    ival= 0 
    for column in columns:
        s = go.Bar(x = dw_source.index.values, y= dw_source[column].values, 
                name = column, 
                marker = {'color': colorarray[ival]},
                )
        data.append(s)
        ival += 1

    BarChart = {'data':data, 'layout':layout}

    BarChart['layout']['autosize'] = True
    #BarChart['layout']['margin']=go.layout.Margin(l=0, r=0, t=30, b=0)

    return BarChart
    #plotly.offline.iplot(BarChart)

if __name__ == "__main__":
    create_map_box_chart_static()